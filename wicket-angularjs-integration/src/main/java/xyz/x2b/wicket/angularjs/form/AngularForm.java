package xyz.x2b.wicket.angularjs.form;

import com.github.openjson.JSONException;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.cycle.RequestCycle;
import xyz.x2b.wicket.angularjs.behavior.AngularControllerBehavior;
import xyz.x2b.wicket.angularjs.behavior.IAngularController;
import xyz.x2b.wicket.angularjs.serializer.IModelSerializer;

import java.io.Serializable;

public abstract class AngularForm<T extends Serializable> extends WebMarkupContainer
        implements IAngularController, IModelSerializer<T> {

    IModelSerializer<T> modelSerializer;

    public AngularForm(String id, T formInput, IModelSerializer<T> modelSerializer) {
        super(id, new Model<>(formInput));
        this.modelSerializer = modelSerializer;
    }

    public AngularForm(String id, IModel<?> model, IModelSerializer<T> modelSerializer) {
        super(id, model);
        this.modelSerializer = modelSerializer;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        add(new AngularControllerBehavior());
        onInitializeForm();
    }

    @Override
    protected void onComponentTag(final ComponentTag tag) {
        checkComponentTag(tag, "form");
        super.onComponentTag(tag);
    }

    @Override
    public JsonElement serialize(IModel<T> model) throws JSONException {
        return modelSerializer.serialize(model);
    }

    @Override
    public T deserialize(JsonObject json) throws JSONException {
        return modelSerializer.deserialize(json);
    }

    @Override
    public String getControllerName() {
        return "Form_" + getId();
    }

    @Override
    public void invoke(RequestCycle requestCycle) {
        processedResponse(new AngularResponse(requestCycle));
    }

    protected void onInitializeForm() {}

    protected void processedResponse(AngularResponse angularResponse) {}

    public abstract Class<T> getTypeClass();

    public IModel<T> getModel(){
        return (IModel<T>) getDefaultModel();
    }

    public T getModelObject(){
        return (T) getDefaultModelObject();
    }
}

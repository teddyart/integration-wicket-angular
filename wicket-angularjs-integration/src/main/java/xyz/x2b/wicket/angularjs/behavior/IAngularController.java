package xyz.x2b.wicket.angularjs.behavior;

import org.apache.wicket.request.cycle.RequestCycle;

public interface IAngularController {
    String getControllerName();
    void invoke(RequestCycle requestCycle);
}

package xyz.x2b.wicket.angularjs.form;

import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;

public class AngularSubmitButton extends WebMarkupContainer {

    public AngularSubmitButton(String id) {
        super(id);
    }

    private void onInitializeButton(){

    }

    @Override
    protected final void onInitialize(){
        super.onInitialize();
        onInitializeButton();
    }

    @Override
    protected void onComponentTag(ComponentTag tag) {
        tag.getAttributes().put("ng:click", "push()");
    }
}

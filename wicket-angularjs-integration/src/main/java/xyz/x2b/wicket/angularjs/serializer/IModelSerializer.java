package xyz.x2b.wicket.angularjs.serializer;

import com.github.openjson.JSONException;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.wicket.model.IModel;

public interface IModelSerializer <T extends Object> {

    JsonElement serialize(IModel<T> model) throws JSONException;
    T deserialize(JsonObject json) throws JSONException;

}

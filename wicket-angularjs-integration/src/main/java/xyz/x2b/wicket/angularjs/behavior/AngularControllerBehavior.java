package xyz.x2b.wicket.angularjs.behavior;

import com.github.openjson.JSONException;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.slf4j.Slf4j;
import org.apache.wicket.Component;
import org.apache.wicket.behavior.AbstractAjaxBehavior;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.model.IModel;
import org.apache.wicket.protocol.http.servlet.ServletWebRequest;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.TextRequestHandler;
import org.apache.wicket.util.template.PackageTextTemplate;
import xyz.x2b.wicket.angularjs.resources.AngularResources;
import xyz.x2b.wicket.angularjs.serializer.IModelSerializer;
import xyz.x2b.wicket.angularjs.utils.Utils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;

@Slf4j
public class AngularControllerBehavior extends AbstractAjaxBehavior {

    private String controllerName;

    @Override
    protected void onComponentTag(ComponentTag tag) {
        Component component = getComponent();
        if(component instanceof IAngularController) {
            IAngularController controller = (IAngularController) component;
            this.controllerName = controller.getControllerName();
        } else {
            this.controllerName = "Controller_" + tag.getId();
        }
        tag.getAttributes().put("ng:controller", this.controllerName);
    }

    @Override
    protected void onComponentRendered() {
        Component component = getComponent();
        JavaScriptHeaderItem
                .forScript(
                        new PackageTextTemplate(AngularResources.class, "template-controller.js")
                                .asString(new HashMap<String, Object>(){
                                    {
                                        put("controller_name", AngularControllerBehavior.this.controllerName);
                                        put("controller_json_url", AngularControllerBehavior.this.getCallbackUrl());
                                    }
                                }),
                        "angular-controller-" + component.getId())
                .render(component.getResponse());
    }

    @Override
    public void onRequest() {
        Component component = getComponent();
        IModel<?> componentModel = component.getDefaultModel();

        RequestCycle cycle = RequestCycle.get();
        ServletWebRequest webRequest = (ServletWebRequest) cycle.getRequest();

        HttpServletRequest request = webRequest.getContainerRequest();
        String method = request.getMethod();

        if(!(component instanceof IModelSerializer)){
            return;
        }

        IModelSerializer serializer = (IModelSerializer) component;

        if("POST".equals(method)){
            String requestBody;
            try {
                requestBody = Utils.readRequestBody(request);

                Object modelValue = serializer.deserialize((JsonObject) new JsonParser().parse(requestBody));
                component.setDefaultModelObject(modelValue);
            } catch(IOException e) {
                log.error(e.getMessage(), e);
                return;
            }
            if(component instanceof IAngularController){
                IAngularController controller = (IAngularController) component;
                controller.invoke(cycle);
            }
        }

        try {
            JsonElement serialized = serializer.serialize(componentModel);
            if(serialized == null) {
                return;
            }
            cycle.scheduleRequestHandlerAfterCurrent(
                    new TextRequestHandler("application/json", "UTF-8", serialized.toString())
            );
        } catch (JSONException e) {
            log.error(e.getMessage(), e);
        }
    }
}

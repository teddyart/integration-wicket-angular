package xyz.x2b.wicket.angularjs.serializer.impl;

import com.github.openjson.JSONException;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.wicket.model.AbstractPropertyModel;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import xyz.x2b.wicket.angularjs.serializer.IModelSerializer;

import java.io.Serializable;

public abstract class DefaultObjectSerializer<T extends Object> implements IModelSerializer<T>, Serializable {

    private static final long serialVersionUID = 1L;
    private static final String DEFAUL_ANGULAR_MODEL_PROPERTY = "data";

    public abstract Class<T> getClassType();

    @Override
    public JsonElement serialize(IModel<T> model) throws JSONException {
        Object value = model.getObject();
        if(value != null) {
            return new Gson().toJsonTree(value);
        }
        return new JsonObject();
    }

    @Override
    public T deserialize(JsonObject json) throws JSONException {
        Gson deserializer = new Gson();
        return deserializer.fromJson(json, getClassType());
    }

    public static IModel<String> getAngularModelProperty(){
        return (IModel<String>) () -> DEFAUL_ANGULAR_MODEL_PROPERTY;
    }
}

package xyz.x2b.wicket.angularjs.form;

import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.TextRequestHandler;

public class AngularResponse {

    private RequestCycle cycle;

    public AngularResponse(RequestCycle requestCycle) {
        this.cycle = requestCycle;
    }

    public void eval(String javascript) {
        cycle.scheduleRequestHandlerAfterCurrent(
                new TextRequestHandler(
                    "text/javascript",
                    "UTF-8",
                    OnDomReadyHeaderItem.forScript(javascript).getJavaScript().toString()
                )
        );
    }

    public void alert(String text) {
        eval(String.format("alert('%s')", text));
    }
}

package xyz.x2b.wicket.angularjs.label;

import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.MarkupStream;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import xyz.x2b.wicket.angularjs.serializer.impl.DefaultObjectSerializer;

public class AngularLabel extends Label {

    private IModel<String> property;

    public AngularLabel(String id, IModel<String> property){
        super(id);
        this.property = property;
    }

    public AngularLabel(String id, String property) {
        this(id, new Model<>(property));
    }

    public AngularLabel(String id) {
        this(id, DefaultObjectSerializer.getAngularModelProperty());
    }

    @Override
    public void onComponentTagBody(MarkupStream markupStream, ComponentTag tag){
        replaceComponentTagBody(markupStream, tag, "{{data."+property.getObject()+"}}");
    }
}
